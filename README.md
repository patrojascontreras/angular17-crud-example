# Angular17-CRUD-Example

## Descripción
Proyecto de desarrollo web realizado en Angular versión 17.1.4 que el cuál consiste en un CRUD de datos vinculados a personas.

### Estructura del proyecto:

![Estructura del proyecto](/images/Angular17_estructure-project-crud-example.jpg) 

### Pasos ejecución y construcción del proyecto: 

**Paso 01:** Creación del proyecto: 

![Paso 01 - 0001](/images/Paso01_Angular17.jpg) 

![Paso 01 - 0002](/images/Paso02_Angular17.jpg) 

![Paso 01 - 0003](/images/Paso03_Angular17.jpg) 

![Paso 01 - 0004](/images/Paso04_Angular17.jpg) 

**Avisos importantes:** 
* Se usa la opción **ng new --no-standalone** para crear un proyecto con **NgModule**. 
* Se aplica la opción **N** mediante comando cmd para deshabilitar el uso de **Server-side rendering (SSR)** y **Static Site Generation (SSG/Prerendering)** dentro del proyecto. 

**Paso 02:** Definición y configuración de librerías a utilizar: 
1. Agregar mediante comandos las siguientes librerías: 

* **npm install bootstrap@5.2.3 --legacy-peer-deps**
* **npm install @ng-bootstrap/ng-bootstrap@14.0.0 --legacy-peer-deps**
* **npm install sweetalert2 --legacy-peer-deps**
* **npm install jquery @popperjs/core --legacy-peer-deps**

2. Configuración de funcionamiento de Bootstrap: 

![Configuración de Bootstrap](/images/Angular17_bootstrap-configuration.jpg) 

**Aviso importante:** Es la forma más eficiente para que Bootstrap funcione correctamente en el proyecto. 

**Paso 03:** Creación de archivos de componentes(**component**), servicios(**service**) y modelos(**model**) vinculados al proyecto mediante los siguientes comandos: 

* **ng generate component components/persons**
* **ng generate class models/person --type=model**
* **ng generate class models/personEntity --type=model**
* **ng generate class models/category --type=model** 
* **ng generate service services/person** 
* **ng generate service services/category** 

**Paso 04:** Inicio del desarrollo mediante definiciones en rutas, componentes, servicios, modelos o interfaces vinculadas al proyecto. 

**Paso 05:** Proceso de despliegue del proyecto: 

![Paso 05 - 0001](/images/Angular17_deployment-0001.jpg) 

![Paso 05 - 0002](/images/Angular17_deployment-0002.jpg) 

**Paso 06:** Ejecución de la aplicación vinculada al proyecto: 

Ingresar al navegador mediante la url http://localhost:4200/

![Paso 06 - 0001](/images/running-application_angular17-0001.jpg) 

![Paso 06 - 0002](/images/running-application_angular17-0002.jpg) 

![Paso 06 - 0003](/images/running-application_angular17-0003.jpg) 

![Paso 06 - 0004](/images/running-application_angular17-0004.jpg) 

![Paso 06 - 0005](/images/running-application_angular17-0005.jpg) 

![Paso 06 - 0006](/images/running-application_angular17-0006.jpg) 

![Paso 06 - 0007](/images/running-application_angular17-0007.jpg) 

![Paso 06 - 0008](/images/running-application_angular17-0008.jpg) 

![Paso 06 - 0009](/images/running-application_angular17-0009.jpg) 

### Avisos importantes: 

* La realización de este proyecto consiste en la migración a Angular 17 del proyecto CRUD-Example que se encuentra mediante versión de Angular 16 dentro del siguiente repositorio: https://gitlab.com/patrojascontreras/angular16-crud-example 
* En caso de que el proyecto se encuentre creado y se haya descargado a otro pc, solamente se debe ejecutar el comando **npm install** para el funcionamiento de las mismas dependencias. 
* Originalmente se aprendió este tipo de versiones de Angular desde que usé Angular 10 a finales del año 2020, pero por motivos de tiempo se pudo realizar en el día de hoy este proyecto. 
* Los datos previamente mostrados en las imagenes de la ejecución de la aplicación provienen desde el Backend de **Java Spring Framework** mediante **Spring Boot** que se encuentra en la versión 2020 del proyecto mediante el siguiente repositorio: https://gitlab.com/patrojascontreras/fullstack_patriciorojas_2020 
* Para la ejecución de test unitarios, se debe usar el siguiente comando: **ng test** 
* Para la ejecución de Build, se debe usar el siguiente comando: **ng build** 
* Con respecto a la ejecución de end-to-end tests, se debe utilizar el siguiente comando: **ng e2e** 
* Para temas de ayuda en Angular CLI dentro de un proyecto, se debe utilizar el siguiente comando: **ng help** 

Realizado por Ing. Patricio Rojas Contreras - 05 de julio de 2024. 

