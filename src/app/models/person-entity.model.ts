export class PersonEntity {
    idPerson!: number;
    name!: string;
    lastName!: string;
    male!: string;
    email!: string;
    categoryName!: string;
}
