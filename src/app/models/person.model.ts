export class Person {
    idPerson!: number;
    name!: string;
    lastName!: string;
    male!: string;
    email!: string;
    personCategory!: number;
}
