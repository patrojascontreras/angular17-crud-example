import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Category } from '../models/category.model';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class CategoryService {

  private urlReadCategoriesListAll = environment.urlReadCategoriesListAll;

  constructor(private http: HttpClient) { }

  getCategoriesListAll(): Observable<Category[]> {
    return this.http.get<Category[]>(this.urlReadCategoriesListAll);
  }
  
}
