import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';

import { Person } from '../../models/person.model';
import { PersonEntity } from '../../models/person-entity.model';
import { PersonService } from '../../services/person.service';

import { Category } from '../../models/category.model';
import { CategoryService } from '../../services/category.service';

import { ActivatedRoute } from '@angular/router';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-persons',
  templateUrl: './persons.component.html',
  styleUrl: './persons.component.css', 
  providers: [PersonService, CategoryService, NgbModalConfig, NgbModal]
})
export class PersonsComponent implements OnInit {
  
  title_persons_management_gral = 'CRUD Gestión de Personas';
  title_persons_management_list = 'Listado de Personas';

  button_title_new_person_create = 'Nueva Persona';
	button_title_person_search = 'Buscar';
	
	grid_table_person_id = 'ID';
	grid_table_person_name = 'Nombre';
	grid_table_person_lastName = 'Apellidos';
	grid_table_person_male = 'Sexo';
	grid_table_person_email = 'Email';
	grid_table_person_category = 'Categoría';
	grid_table_person_actions = 'Acciones';

  formPersonCancelBtn = 'Cancelar';
  formPersonCreateBtn = 'Registrar';
  formPersonUpdateBtn = 'Modificar';
  formPersonDeleteBtn = 'Eliminar';
  formPersonDetailsCloseBtn = 'Cerrar';
  
  titleFormPersonCreate = 'Formulario Registro Persona';
  titleFormPersonUpdate = 'Formulario Edición Persona';
  titleFormPersonDelete = 'Confirmar Eliminar';
  titleFormPersonDetails = 'Detalles de Persona';
  
  textDescriptionModalDeletePerson = '¿Desea eliminar este registro?';
  
  formPersonId = 'ID';
  formPersonName = 'Nombre';
  formPersonLastName = 'Apellidos';
  formPersonMale = 'Sexo';
  formPersonEmail = 'Email';
  formPersonCategory = 'Categoría';

  //Variable para realizar la busqueda por ID
  searchingId: any;
  //Fin variable para realizar la busqueda por ID

  //Variable para la muestra de mensaje(s)
  messageResponse: string = '';
  //Fin ariable para la muestra de mensaje(s)

  //Variables para registrar Persona
  formPersonCreate = {
    name: '', 
    lastName: '', 
    male: '', 
    email: '', 
    category: 0
  };
  //Fin variables para registrar Persona

  //Variables para edición y modificación de Persona
  formPersonEdit: Person = new Person();

  idPersonAntidisabled: number = 0;
  //Fin variables para edición y modificación de Persona

  //Variable para mostrar vista Detalles de Persona
  readPersonDetails: PersonEntity = new PersonEntity();
  //Fin variable para mostrar vista Detalles de Persona

  //Variable para usar en proceso de eliminación de Persona
  personIdDelete: number = 0;
  //Fin variable para usar en proceso de eliminación de Persona

  //Uso de decorador @ViewChild para poder acceder a una directiva de la manera mas eficiente, en este caso: un modal.
  @ViewChild('myModalPersonCreate') myModalPersonCreate!: TemplateRef<any>;
  @ViewChild('myModalPersonUpdate') myModalPersonUpdate!: TemplateRef<any>;
  @ViewChild('myModalPersonDeletePreviusConfirm') myModalPersonDeletePreviusConfirm!: TemplateRef<any>;
  @ViewChild('myModalPersonDataView') myModalPersonDataView!: TemplateRef<any>;
  //Fin uso de decorador @ViewChild

  //Variables respuestas de campos y/o combobox inválidos
  errorMessageInvalidName = 'Por favor, ingrese un nombre válido';
  errorMessageInvalidLastName = 'Por favor, ingrese un apellido(s) válido';
  errorMessageInvalidEmail = 'Por favor, ingrese un email válido';

  errorMessageMinLengthName = 'El campo Nombre debe tener al menos 2 letras';
  errorMessageMinLengthLastName = 'El campo Apellido(s) debe tener al menos 2 letras';
  //Fín Variables respuestas de campos y/o combobox inválidos
  
  prsListAll!: PersonEntity[];
  ctsListAll!: Category[];

  constructor(private activatedRoute: ActivatedRoute, private personService: PersonService, private categoryService: CategoryService, config: NgbModalConfig, private modalService: NgbModal) {
    config.backdrop = 'static';
		config.keyboard = false;
  }

  ngOnInit(): void {
    this.getPersonsListAll();
  }

  getPersonsListAll(): void {

    this.personService.getPersonsListAll().subscribe(data => {
      this.prsListAll = data;
      console.log(this.prsListAll);
    }, error => {
      if(error.status == 404) {
        this.prsListAll = [];
        this.messageResponse = 'No hay datos encontrados';
        console.log(this.messageResponse);
      } else if(error.status == 500) {
        this.messageResponse = 'Error interno en el servidor';
        console.log(this.messageResponse);

        Swal.fire({
          toast: true,
          position: 'bottom',
          showConfirmButton: false,
          icon: 'error',
          timerProgressBar: false,
          timer: 5000,
          title: this.messageResponse
        });
      }
    });
  }

  getPersonsListAllById(): void {

    const regexIdValidation = /[0-9]+$/;

    if(!regexIdValidation.test(this.searchingId)) {

      Swal.fire({
        toast: true,
        position: 'bottom',
        showConfirmButton: false,
        icon: 'error',
        timerProgressBar: false,
        timer: 5000,
        title: 'El campo ID no posee los carácteres válidos'
      });
    }

    this.personService.getPersonsListAllById(this.searchingId).subscribe(data => {
      this.prsListAll = data;
      console.log(this.prsListAll);

      this.searchingId = '';
    }, error => {
      if(error.status == 404) {
        this.searchingId = '';
        this.prsListAll = [];
        this.messageResponse = 'No hay datos encontrados';
        console.log(this.messageResponse);
      } else if(error.status == 500) {
        this.messageResponse = 'Error interno en el servidor';
        console.log(this.messageResponse);

        Swal.fire({
          toast: true,
          position: 'bottom',
          showConfirmButton: false,
          icon: 'error',
          timerProgressBar: false,
          timer: 5000,
          title: this.messageResponse
        });
      }
    });
  }

  getCategoriesListAll(): void {

    this.categoryService.getCategoriesListAll().subscribe(data => {
      this.ctsListAll = data;
      console.log(data);
    }, error => {
      if(error.status == 404) {
        this.ctsListAll = [];
        this.messageResponse = 'No hay datos encontrados';
        console.log(this.messageResponse);

        Swal.fire({
          toast: true,
          position: 'bottom',
          showConfirmButton: false,
          icon: 'error',
          timerProgressBar: false,
          timer: 5000,
          title: this.messageResponse
        });
      } else if(error.status == 500) {
        this.messageResponse = 'Error interno en el servidor';
        console.log(this.messageResponse);

        Swal.fire({
          toast: true,
          position: 'bottom',
          showConfirmButton: false,
          icon: 'error',
          timerProgressBar: false,
          timer: 5000,
          title: this.messageResponse
        });
      }
    });
  }

  openFormPersonCreate(): void {
    this.clearInputsFormPersonCreate();

    this.getCategoriesListAll();
    this.modalService.open(this.myModalPersonCreate);
  }

  cancelPersonCreate(): void {
    this.modalService.dismissAll();
  }

  personCreate(): void {

    const dataPerson = {
      'name': this.formPersonCreate.name, 
      'lastName': this.formPersonCreate.lastName, 
      'male': this.formPersonCreate.male, 
      'email': this.formPersonCreate.email, 
      'personCategory': this.formPersonCreate.category
    };

    this.personService.personCreate(dataPerson).subscribe(data => {
      
      this.messageResponse = 'Dato registrado correctamente';
      console.log(this.messageResponse);

      Swal.fire({
        toast: true,
        position: 'bottom',
        showConfirmButton: false,
        icon: 'success',
        timerProgressBar: false,
        timer: 5000,
        title: this.messageResponse
      });
      
      this.getPersonsListAll();
      this.modalService.dismissAll();

    }, error => {
      if(error.status == 500) {
        this.messageResponse = 'Error interno en el servidor';
        console.log(this.messageResponse);

        Swal.fire({
          toast: true,
          position: 'bottom',
          showConfirmButton: false,
          icon: 'error',
          timerProgressBar: false,
          timer: 5000,
          title: this.messageResponse
        });
      }
    });
  }

  clearInputsFormPersonCreate(): void {
    this.formPersonCreate.name = '';
    this.formPersonCreate.lastName = '';
    this.formPersonCreate.male = '';
    this.formPersonCreate.email = '';
    this.formPersonCreate.category = 0;

    this.disabledGetEmailWithDetails = true;
  }

  personEdit(idPerson: number): void {
    this.personService.personEdit(idPerson).subscribe(data => {

      this.formPersonEdit.idPerson = data.idPerson;
      this.formPersonEdit.name = data.name;
      this.formPersonEdit.lastName = data.lastName;
      this.formPersonEdit.male = data.male;
      this.formPersonEdit.email = data.email;
      this.formPersonEdit.personCategory = data.personCategory;

      this.idPersonAntidisabled = data.idPerson;

      console.log(data);

      this.getCategoriesListAll();
      this.modalService.open(this.myModalPersonUpdate);
    }, error => {

      if(error.status == 404) {
        this.messageResponse = 'Dato no encontrado';
        console.log(this.messageResponse);
      } else if(error.status == 500) {
        this.messageResponse = 'Error interno en el servidor';
        console.log(this.messageResponse);
      }
    });
  }

  personUpdate(): void {

    var dataPerson = {
      'idPerson': this.idPersonAntidisabled, 
      'name': this.formPersonEdit.name, 
      'lastName': this.formPersonEdit.lastName, 
      'male': this.formPersonEdit.male, 
      'email': this.formPersonEdit.email, 
      'personCategory': this.formPersonEdit.personCategory
    };

    this.personService.personUpdate(dataPerson).subscribe(data => {
      this.messageResponse = 'Dato modificado correctamente';
      console.log(this.messageResponse);

      Swal.fire({
        toast: true,
        position: 'bottom',
        showConfirmButton: false,
        icon: 'success',
        timerProgressBar: false,
        timer: 5000,
        title: this.messageResponse
      });

      this.getPersonsListAll();
      this.modalService.dismissAll();

    }, error => {
      if(error.status == 404) {
        this.messageResponse = 'Dato no encontrado';
        console.log(this.messageResponse);

        Swal.fire({
          toast: true,
          position: 'bottom',
          showConfirmButton: false,
          icon: 'error',
          timerProgressBar: false,
          timer: 5000,
          title: this.messageResponse
        });
      } else if(error.status == 500) {
        this.messageResponse = 'Error interno en el servidor';
        console.log(this.messageResponse);

        Swal.fire({
          toast: true,
          position: 'bottom',
          showConfirmButton: false,
          icon: 'error',
          timerProgressBar: false,
          timer: 5000,
          title: this.messageResponse
        });
      }
    });
  }

  cancelPersonUpdate(): void {
    this.modalService.dismissAll();
  }

  openModalPersonDelete(idPerson: number): void {
    this.personIdDelete = idPerson;
    console.log(this.personIdDelete);
    this.modalService.open(this.myModalPersonDeletePreviusConfirm);
  }

  confirmPersonDelete(): void {
    this.personService.personDelete(this.personIdDelete).subscribe(data => {
      this.messageResponse = 'Dato eliminado correctamente';
      console.log(this.messageResponse);

      Swal.fire({
        toast: true,
        position: 'bottom',
        showConfirmButton: false,
        icon: 'success',
        timerProgressBar: false,
        timer: 5000,
        title: this.messageResponse
      });

      this.getPersonsListAll();
      this.modalService.dismissAll();
    }, error => {
      if(error.status == 404) {
        this.messageResponse = 'Dato no encontrado';
        console.log(this.messageResponse);

        Swal.fire({
          toast: true,
          position: 'bottom',
          showConfirmButton: false,
          icon: 'error',
          timerProgressBar: false,
          timer: 5000,
          title: this.messageResponse
        });
      } else if(error.status == 500) {
        this.messageResponse = 'Error interno en el servidor';
        console.log(this.messageResponse);

        Swal.fire({
          toast: true,
          position: 'bottom',
          showConfirmButton: false,
          icon: 'error',
          timerProgressBar: false,
          timer: 5000,
          title: this.messageResponse
        });
      }
    });
  }

  cancelPersonDelete(): void {
    this.modalService.dismissAll();
  }

  openModalPersonDetails(idPerson: number): void {
    this.personService.personDetails(idPerson).subscribe(data => {
      this.readPersonDetails = data;
      console.log(this.readPersonDetails);

      this.modalService.open(this.myModalPersonDataView);
    }, error => {

      if(error.status == 404) {
        this.messageResponse = 'Dato no encontrado';
        console.log(this.messageResponse);

        Swal.fire({
          toast: true,
          position: 'bottom',
          showConfirmButton: false,
          icon: 'error',
          timerProgressBar: false,
          timer: 5000,
          title: this.messageResponse
        });
      } else if(error.status == 500) {
        this.messageResponse = 'Error interno en el servidor';
        console.log(this.messageResponse);

        Swal.fire({
          toast: true,
          position: 'bottom',
          showConfirmButton: false,
          icon: 'error',
          timerProgressBar: false,
          timer: 5000,
          title: this.messageResponse
        });
      }
    });
  }

  closeModalPersonDetails(): void {
    this.modalService.dismissAll();
  }

  //Funciones para validar determinado(s) campo(s)
  disabledGetEmailWithDetails = true;

  disabledInvalidPatternEmail = true;

  disabledCheckEmail = true;

  errorMessageCheckEmailPersonExist: string = '';

  getInvalidEmailWithDetails(e: any): void {
    this.disabledGetEmailWithDetails = false;

    const regexEmailValidation = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if(!regexEmailValidation.test(e)) {
      this.disabledInvalidPatternEmail = false;
    } else {

      //Verificar existencia de email
      this.personService.checkEmailExist(e).subscribe(data => {
        this.disabledCheckEmail = false;
        this.errorMessageCheckEmailPersonExist = 'Ya existe el Email de la persona';
        console.log(this.errorMessageCheckEmailPersonExist);

      }, error => {
        this.disabledGetEmailWithDetails = true;

        if(error.status == 404) {
          this.disabledInvalidPatternEmail = true;
          this.disabledCheckEmail = true;

          this.messageResponse = 'Email disponible a registrar';
          console.log(this.messageResponse);
        } else if(error.status == 500) {
          this.disabledInvalidPatternEmail = true;
          this.disabledCheckEmail = true;
          
          this.messageResponse = 'Error interno en el servidor';
          console.log(this.messageResponse);
        }
      });
      //Fin verificar existencia de email
    }
  }
  //Fín funciones para validar determinado(s) campo(s)
}
